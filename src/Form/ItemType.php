<?php

namespace App\Form;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Formulario para los correos de contacto.
 *
 * @author Enrique José Esteban Plaza <ense.esteban@gmail.com>
 */
class ItemType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array(
            'attr' => array('required' => 'required'),
        ));
        $builder->add('description', CKEditorType::class);
        $builder->add('typePin', ChoiceType::class, array(
            'choices'  => array (
                'Pin' => 'Pin',
                'Password' => 'Pass',
                'Patrón' => 'Patron',
            ),
        ));
        $builder->add('pin', TextType::class, array(
            'attr' => array('required' => 'required'),
        ));
        $builder->add('observations', CKEditorType::class, array(
            'attr' => array('required' => 'required'),
        ));
        $builder->add('cost', MoneyType::class);
        $builder->add('advance', MoneyType::class);
        $builder->add('discount', PercentType::class, array (
            'type' => 'integer',
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Item',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'App_item';
    }
}