<?php

namespace App\Controller;

use App\Entity\Employee;
use Doctrine\DBAL\DBALException;
use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Controlador personalizado de EasyAdminBundle para integrar FOSUserBundle.
 *
 * @author Enrique José Esteban Plaza <ense.esteban@gmail.com>
 */
class AdminController extends BaseAdminController
{
    public function createNewEmployeeEntity()
    {
        return $this->get('fos_user.user_manager')->createUser();
    }

    /**
     * @param Employee
     */
    public function persistEmployeeEntity($employee)
    {
        $this->get('fos_user.user_manager')->updateUser($employee, false);
        parent::persistEntity($employee);
    }

    /**
     * @param Employee
     */
    public function updateEmployeeEntity($employee)
    {
        $this->get('fos_user.user_manager')->updateUser($employee, false);
        parent::updateEntity($employee);
    }

    /**
     * @param $entity
     */
    protected function persistInvoiceEntity($entity)
    {
        $em = $this->getDoctrine()->getManager();
        $date = new \DateTime('now');
        $year = $date->format('Y');

        $invoices = $em->getRepository('App:Invoice')->findByYear($year);

        $maxNum = 1;
        foreach ($invoices as $invoice) {
            $numCode = (int) substr($invoice->getCode(), -5);
            if ($numCode > $maxNum) {
                $maxNum = $numCode;
            }
        }

        $entity->setCode($year.'/'.str_pad($maxNum + 1,'5','0',STR_PAD_LEFT));
    }

    protected function createReceiptSearchQueryBuilder($entityClass, $searchQuery, array $searchableFields, $sortField = null, $sortDirection = null, $dqlFilter = null)
    {
        $em = $this->getDoctrine()->getManagerForClass($entityClass);

        $queryBuilder = $em->createQueryBuilder()
            ->select('entity')
            ->from($entityClass, 'entity')
        ;

        if (substr_count($searchQuery, ':') == 0) {
            // Buscamos en los campos base
            foreach ($searchableFields as $searchableField) {
                $queryBuilder->orWhere('LOWER(entity.'.$searchableField['fieldName'].') LIKE :query');
            }

            // Buscamos también en los items asociados a la entidad
            $queryBuilder->join('entity.items', 'items')
                ->orWhere('LOWER(items.name) LIKE :query')
            ;

            // Buscamos también en es estado asociado
            $queryBuilder->join('entity.state', 'state')
                ->orWhere('LOWER(state.name) LIKE :query')
            ;
        }
        elseif (substr_count($searchQuery, ':') === 1 && strpos($searchQuery, ':') >= 3) {
            $queryFlag = substr($searchQuery, 0, strpos($searchQuery, ':'));
            $searchQuery = substr($searchQuery, strpos($searchQuery, ':') + 1);

            switch ($queryFlag) {
                case 'cod': // entity.code
                    $queryBuilder->orWhere('LOWER(entity.code) LIKE :query');
                    break;
                case 'nom': // entity.clientName
                    $queryBuilder->orWhere('LOWER(entity.clientName) LIKE :query');
                    break;
                case 'tel': // entity.clientPhone
                    $queryBuilder->orWhere('LOWER(entity.clientPhone) LIKE :query');
                    break;
                case 'mail': // entity.clientEmail
                    $queryBuilder->orWhere('LOWER(entity.clientEmail) LIKE :query');
                    break;
                case 'dir': // entity.clientAddress
                    $queryBuilder->orWhere('LOWER(entity.clientAddress) LIKE :query');
                    break;
                case 'item': // entity.items
                    $queryBuilder->join('entity.items', 'items')
                        ->orWhere('LOWER(items.name) LIKE :query')
                    ;
                    break;
                case 'estado': // entity.state
                    $queryBuilder->join('entity.state', 'state')
                        ->orWhere('LOWER(state.name) LIKE :query')
                    ;
                    break;
                default:
                    throw  new BadRequestHttpException('La bandera no coincide con ningún patrón válido', null,'400');
            }

        }
        else {
            throw  new BadRequestHttpException('Patrón de búsqueda no válido', null,'400');
        }

         $queryBuilder->setParameter('query', '%'.strtolower($searchQuery).'%');

        if (!empty($dqlFilter)) {
            $queryBuilder->andWhere($dqlFilter);
        }

        if (null !== $sortField) {
            $queryBuilder->orderBy('entity.'.$sortField, $sortDirection ?: 'DESC');
        }

        return $queryBuilder;
    }

    protected function createInvoiceSearchQueryBuilder($entityClass, $searchQuery, array $searchableFields, $sortField = null, $sortDirection = null, $dqlFilter = null)
    {
        return $this->createReceiptSearchQueryBuilder($entityClass, $searchQuery, $searchableFields, $sortField, $sortDirection, $dqlFilter);
    }
}
