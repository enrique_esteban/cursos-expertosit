<?php

namespace App\DataFixtures;

use App\Entity\Entry;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadEntries extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Entrada
        $entry = new Entry();

        $entry->setCategory($this->getReference('cat-youtube'));
        $entry->setTitle('Introducción al diagnostico de terminales: Fuente portátil.');
        $entry->setSlug('introduccion-al-diagnostico-de-terminales-fuente-portatil');
        $entry->setContent('<p>En este video vemos como diagnosticar un terminal con problemas de encendido o que no da ninguna señal de vida. (ni enciende ni carga)</p>');
        $entry->setEmbed('HmcGA_8tvrQ');
        $entry->setImage(null);
        $entry->setIsPrivated(false);
        $entry->setCreateAt(new \DateTime('2018-04-13'));
        $entry->addTag($this->getReference('tag-youtube'));
        $entry->addTag($this->getReference('tag-video'));
        $entry->addTag($this->getReference('tag-curso'));
        $entry->addTag($this->getReference('tag-clase'));
        $entry->addTag($this->getReference('tag-reparacion'));
        $entry->addTag($this->getReference('tag-diagnostico'));
        $entry->addTag($this->getReference('tag-fuente-alimentacion'));
        $entry->addTag($this->getReference('tag-fuente-alimentacion-portatil'));
        $entry->addTag($this->getReference('tag-herramienta'));


        $manager->persist($entry); // Fin de la entrada

        // Entrada
        $entry = new Entry();

        $entry->setCategory($this->getReference('cat-youtube'));
        $entry->setTitle('¿Porqué los botones home dejan de funcionar después de cambiarlos?');
        $entry->setSlug('porque-los-botones-home-dejan-de-funcionar-despues-de-cambiarlos');
        $entry->setContent('<p>Clase sobre botones home iPhone 7 y explicación de porque no funcionan después de cambiarlos.</p>');
        $entry->setEmbed('Z5VXsE7tNzU');
        $entry->setImage(null);
        $entry->setIsPrivated(false);
        $entry->setCreateAt(new \DateTime('2018-03-07'));
        $entry->addTag($this->getReference('tag-youtube'));
        $entry->addTag($this->getReference('tag-video'));
        $entry->addTag($this->getReference('tag-curso'));
        $entry->addTag($this->getReference('tag-clase'));
        $entry->addTag($this->getReference('tag-reparacion'));
        $entry->addTag($this->getReference('tag-iphone'));
        $entry->addTag($this->getReference('tag-iphone-7'));
        $entry->addTag($this->getReference('tag-boton-home'));

        $manager->persist($entry); // Fin de la entrada

        // Entrada
        $entry = new Entry();

        $entry->setCategory($this->getReference('cat-youtube'));
        $entry->setTitle('Diagnóstico de problemas de carga en Iphone');
        $entry->setSlug('diagnostico-de-problemas-de-carga-en-iphone');
        $entry->setContent('<p>Dock flexo test. Modalidad de diagnosticar problemas de carga en IPhone.</p>');
        $entry->setEmbed('ibgJPbs78o0');
        $entry->setImage(null);
        $entry->setIsPrivated(false);
        $entry->setCreateAt(new \DateTime('2018-02-18'));
        $entry->addTag($this->getReference('tag-youtube'));
        $entry->addTag($this->getReference('tag-video'));
        $entry->addTag($this->getReference('tag-curso'));
        $entry->addTag($this->getReference('tag-clase'));
        $entry->addTag($this->getReference('tag-reparacion'));
        $entry->addTag($this->getReference('tag-iphone'));
        $entry->addTag($this->getReference('tag-diagnostico'));
        $entry->addTag($this->getReference('tag-dock-test'));
        $entry->addTag($this->getReference('tag-herramienta'));

        $manager->persist($entry); // Fin de la entrada

        // Entrada
        $entry = new Entry();

        $entry->setCategory($this->getReference('cat-youtube'));
        $entry->setTitle('Cambio de cristal protector de cámara sin necesidad de desarmar el iPhone 7');
        $entry->setSlug('cambio-de-cristal-protector-de-camara-sin-necesidad-de-desarmar-el-iphone-7');
        $entry->setContent('<p></p>');
        $entry->setEmbed('m70bsnLviZ4');
        $entry->setImage(null);
        $entry->setIsPrivated(false);
        $entry->setCreateAt(new \DateTime('2018-02-27'));
        $entry->addTag($this->getReference('tag-youtube'));
        $entry->addTag($this->getReference('tag-video'));
        $entry->addTag($this->getReference('tag-curso'));
        $entry->addTag($this->getReference('tag-clase'));
        $entry->addTag($this->getReference('tag-reparacion'));
        $entry->addTag($this->getReference('tag-iphone'));
        $entry->addTag($this->getReference('tag-iphone-7'));
        $entry->addTag($this->getReference('tag-camara'));

        $manager->persist($entry); // Fin de la entrada

        // Entrada
        $entry = new Entry();

        $entry->setCategory($this->getReference('cat-youtube'));
        $entry->setTitle('Detección corto en placa con Seek Thermal');
        $entry->setSlug('deteccion-corto-en-placa-con-seek-thermal');
        $entry->setContent('<p>Detectamos componentes en corto que muestren un calentamiento elevado con la herramienta profesional Seek Thermal.</p>');
        $entry->setEmbed('fH9DC6iCNwo');
        $entry->setImage(null);
        $entry->setIsPrivated(false);
        $entry->setCreateAt(new \DateTime('2018-02-15'));
        $entry->addTag($this->getReference('tag-youtube'));
        $entry->addTag($this->getReference('tag-video'));
        $entry->addTag($this->getReference('tag-curso'));
        $entry->addTag($this->getReference('tag-clase'));
        $entry->addTag($this->getReference('tag-reparacion'));
        $entry->addTag($this->getReference('tag-camara'));
        $entry->addTag($this->getReference('tag-camara-termica'));
        $entry->addTag($this->getReference('tag-seek-thermal'));
        $entry->addTag($this->getReference('tag-corto'));
        $entry->addTag($this->getReference('tag-herramienta'));

        $manager->persist($entry); // Fin de la entrada

        // Entrada
        $entry = new Entry();

        $entry->setCategory($this->getReference('cat-youtube'));
        $entry->setTitle('Comprobación de fallos en el lector micro-sd');
        $entry->setSlug('comprobacion-de-fallos-en-el-lector-micro-sd');
        $entry->setContent('<p>En la clase de hoy aprendemos como vamos a comprobar que la linea detectora de la SD esté funcionando.</p>');
        $entry->setEmbed('9_ZOfEgT8iM');
        $entry->setImage(null);
        $entry->setIsPrivated(false);
        $entry->setCreateAt(new \DateTime('2018-01-30'));
        $entry->addTag($this->getReference('tag-youtube'));
        $entry->addTag($this->getReference('tag-video'));
        $entry->addTag($this->getReference('tag-curso'));
        $entry->addTag($this->getReference('tag-clase'));
        $entry->addTag($this->getReference('tag-reparacion'));
        $entry->addTag($this->getReference('tag-micro-sd'));

        $manager->persist($entry); // Fin de la entrada

        // Entrada
        $entry = new Entry();

        $entry->setCategory($this->getReference('cat-youtube'));
        $entry->setTitle('Introducción Clase Display iPhone');
        $entry->setSlug('inroduccion-clase-display-iphone');
        $entry->setContent('<p>Comprobamos, mediante la localización del ánodo y el cátodo, que funcione correctamente la capa de backlights de las pantallas IPS de los iPhone. Con ello podemos comprobar el correcto funcionamiento del display.</p>');
        $entry->setEmbed('0ae-MK3FKG4');
        $entry->setImage(null);
        $entry->setIsPrivated(false);
        $entry->setCreateAt(new \DateTime('2018-01-26'));
        $entry->addTag($this->getReference('tag-youtube'));
        $entry->addTag($this->getReference('tag-video'));
        $entry->addTag($this->getReference('tag-curso'));
        $entry->addTag($this->getReference('tag-clase'));
        $entry->addTag($this->getReference('tag-reparacion'));
        $entry->addTag($this->getReference('tag-iphone'));
        $entry->addTag($this->getReference('tag-backlight'));
        $entry->addTag($this->getReference('tag-pantalla'));

        $manager->persist($entry); // Fin de la entrada

        // Entrada
        $entry = new Entry();

        $entry->setCategory($this->getReference('cat-youtube'));
        $entry->setTitle('Curso de reparación de móviles');
        $entry->setSlug('curso-de-reparacion-de-moviles');
        $entry->setContent('<p>Nuestros alumnos diagnosticando iphone</p>');
        $entry->setEmbed('fbv3iiYclC4');
        $entry->setImage(null);
        $entry->setIsPrivated(false);
        $entry->setCreateAt(new \DateTime('2018-01-21'));
        $entry->addTag($this->getReference('tag-youtube'));
        $entry->addTag($this->getReference('tag-video'));
        $entry->addTag($this->getReference('tag-curso'));
        $entry->addTag($this->getReference('tag-clase'));
        $entry->addTag($this->getReference('tag-reparacion'));
        $entry->addTag($this->getReference('tag-alumnos'));

        $manager->persist($entry); // Fin de la entrada

        // Entrada
        $entry = new Entry();

        $entry->setCategory($this->getReference('cat-youtube'));
        $entry->setTitle('Reballing IC táctil iPhone 6');
        $entry->setSlug('reballing-ic-tactil-iphone-6');
        $entry->setContent('<p>Ejemplo de reballing del IC del táctil de un iPhone 6</p>');
        $entry->setEmbed('HugSOKHPlJU');
        $entry->setImage(null);
        $entry->setIsPrivated(false);
        $entry->setCreateAt(new \DateTime('2017-10-06'));
        $entry->addTag($this->getReference('tag-youtube'));
        $entry->addTag($this->getReference('tag-video'));
        $entry->addTag($this->getReference('tag-curso'));
        $entry->addTag($this->getReference('tag-clase'));
        $entry->addTag($this->getReference('tag-reparacion'));
        $entry->addTag($this->getReference('tag-reballing'));
        $entry->addTag($this->getReference('tag-ic'));
        $entry->addTag($this->getReference('tag-iphone'));
        $entry->addTag($this->getReference('tag-iphone-6'));

        $manager->persist($entry); // Fin de la entrada

        // Entrada
        $entry = new Entry();

        $entry->setCategory($this->getReference('cat-youtube'));
        $entry->setTitle('Reparación de un dispositivo Samsung que no enciende');
        $entry->setSlug('reparacion-de-un-dispositivo-samsung-que-no-enciende');
        $entry->setContent('<p>Diagnóstico de teléfono Samgung que llega a nuestra tienda porque no enciende.</p>');
        $entry->setEmbed('wwAUEF0RYCk');
        $entry->setImage(null);
        $entry->setIsPrivated(false);
        $entry->setCreateAt(new \DateTime('2017-05-22'));
        $entry->addTag($this->getReference('tag-youtube'));
        $entry->addTag($this->getReference('tag-video'));
        $entry->addTag($this->getReference('tag-curso'));
        $entry->addTag($this->getReference('tag-clase'));
        $entry->addTag($this->getReference('tag-reparacion'));
        $entry->addTag($this->getReference('tag-diagnostico'));
        $entry->addTag($this->getReference('tag-samsung'));

        $manager->persist($entry); // Fin de la entrada

        // Entrada
        $entry = new Entry();

        $entry->setCategory($this->getReference('cat-youtube'));
        $entry->setTitle('Curso de reparación de móviles');
        $entry->setSlug('curso-de-reparacion-de-moviles');
        $entry->setContent('<p>Ejemplo de clase impartida por uno de nuestros profesores.</p>');
        $entry->setEmbed('-wPt9wdx59w');
        $entry->setImage(null);
        $entry->setIsPrivated(false);
        $entry->setCreateAt(new \DateTime('2017-04-03'));
        $entry->addTag($this->getReference('tag-youtube'));
        $entry->addTag($this->getReference('tag-video'));
        $entry->addTag($this->getReference('tag-curso'));
        $entry->addTag($this->getReference('tag-clase'));
        $entry->addTag($this->getReference('tag-alumnos'));

        $manager->persist($entry); // Fin de la entrada

        $manager->flush();
    }

    function getDependencies()
    {
        return array(
            LoadCategories::class,
            LoadTags::Class
        );
    }
}