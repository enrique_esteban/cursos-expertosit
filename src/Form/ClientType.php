<?php

namespace App\Form;

use App\Entity\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as TYPE;

class ClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', @TYPE\TextType::class, array(
                'label' => 'Nombre'
            ))
            ->add('phone', @TYPE\TextType::class, array(
                'label' => 'Teléfono'
            ))
            ->add('email', @TYPE\EmailType::class, array(
                'label' => 'E-mail'
            ))
            ->add('submit', @TYPE\SubmitType::class, array(
                'label' => 'Enviar',
//                'disabled' => true
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Client::class,
        ]);
    }
}
