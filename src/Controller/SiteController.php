<?php
namespace App\Controller;

use App\Entity\Client;
use App\Entity\Entry;
use App\Entity\Image;
use App\Entity\ContactMail;
use App\Entity\Receipt;
use App\Form\ClientType;
use App\Form\ContactMailType;
use App\Form\RepairQueryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SiteController extends Controller
{
    public function index ()
    {
        $videos = $this->getDoctrine()->getRepository(Entry::class)->findRecentYoutubeVideos();

        return $this->render('index.html.twig', array(
            'videos' => $videos,
        ));
    }

    public function mobileCourse ($slug)
    {
        return $this->render('curso-'.$slug.'.html.twig');
    }

    public function contact(Request $request)
    {
        $mail = new ContactMail();

        $form = $this->createForm(ContactMailType::class, $mail);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mail = $form->getData();

            $content = sprintf(" Remitente: %s \n\n Email:\n %s \n\n Mensaje:\n %s \n\n Dirección IP: %s \n",
                $mail->getName(),
                $mail->getEmail(),
                htmlspecialchars($mail->getBody()),
                $request->server->get('REMOTE_ADDR')
            );

            $to = 'cursos@expertosit.es';
            $subject = $mail->getSubject();

            // Debido a una restricción de 1and1 se usa la función mail de php
            $headers = 'From: ' . $mail->getEmail() . PHP_EOL;
            $sended = mail( $to, $subject, $content, $headers );

            if ($sended) {
                $this->addFlash('success', 'El mensaje de <strong class="flash">'.$mail->getEmail().'</strong> sido enviado correctamente.');
            }
            else {
                $this->addFlash('error', 'El mensaje de <strong class="flash">'.$mail->getEmail().'</strong> no ha podido ser enviado.');
            }

//            return $this->render('contacto.html.twig', array(
//                'form' => $form->createView(),
//            ));

            return $this->redirectToRoute('contact');
        }

        return $this->render('contacto.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function legal()
    {
        return $this->render('legal.html.twig', array());
    }

    public function portafolio()
    {
        $videos = $this->getDoctrine()->getRepository(Entry::class)->findYoutubeVideos();
        $images = $this->getDoctrine()->getRepository(Image::class)->findWithTags();

        $classifiedImages = $this->classifyImages($images);

        return $this->render('portafolio.html.twig', array(
            'videos' => $videos,
            'imagesArray' => $classifiedImages
        ));
    }

    public function computerCourse()
    {
        return $this->render('curso-iniciacion-informatica.html.twig');
    }

    public function symfonyCourse()
    {
        return $this->render('curso-symfony-4.html.twig');
    }

    private function classifyImages ($images)
    {
        $classifiedImages = array();

        foreach ($images as $image) {
            $checked = false;

            foreach ($classifiedImages as $classifiedClave => $classifiedImage) {
                $checked = false;
                if (substr($classifiedImage[0]->getUrl(), 0, -7) == substr($image->getUrl(), 0, -7)) {
                    $classifiedImages[$classifiedClave][] = $image;
                    $checked = true;
                }
            }

            if ($checked == false) {
                $classifiedImages[][] = $image;
            }
        }

        return $classifiedImages;
    }

    public function sitemap()
    {
        return $this->render('sitemap.xml.twig');
    }

    public function singup(Request $request)
    {
        $client = new Client();

        $form = $this->createForm(ClientType::class, $client);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $client = $form->getData();
            $client->setCreateAt(new \DateTime('now'));

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($client);
            $entityManager->flush();

            unset($form);
            unset($client);
            $client = new Client();
            $form = $this->createForm(ClientType::class, $client);

            $this->addFlash('success', 'o correctamente.');
        }

        return $this->render('singup.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Muestra un recibo en formato HTML
     *
     * @param Request $request
     * @return Response
     */
    public function showReceiptHtml(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $id = $request->query->get('id');

        $receipt = $em->getRepository('App:Receipt')->findOneById($id);

        if (!$receipt) {
            $this->createNotFoundException();
        }

        return $this->render('receipt-a4.html.twig', array('receipt' => $receipt ));
    }

    /**
     * Muestra una factura en formato HTML
     */
    public function showInvoiceHtml(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $id = $request->query->get('id');

        $invoice = $em->getRepository('App:Invoice')->findOneById($id);

        if (!$invoice) {
            $this->createNotFoundException();
        }

        return $this->render('invoice-a4.html.twig', array('invoice' => $invoice ));
    }

    /**
     * Ofrece un formulario para ingresar un codigo de un reparacion
     */
    public function repairQuery(Request $request)
    {
        $repair = new Receipt();

        $form = $this->createForm(RepairQueryType::class, $repair);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {
                $code = $form->getData()->getCode();

                if (!$code) {
                    $this->addFlash('error', 'Ha ocurrido un error desconocido, no se ha podido completar la búsqueda');

                    return $this->redirectToRoute('repair-query');
                }

                return $this->redirectToRoute('repair_show', array(
                    'code' => $code
                ));
            }
        }

        return $this->render('consulta-de-reparacion.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Procesa un código código de reparación y luego muestra los resultados
     */
    public function repairShow($code)
    {
        $em = $this->getDoctrine()->getManager();
        $receipt = $em->getRepository('App:Receipt')->findOneByCode($code);

        if ($receipt === null) {
            $this->addFlash('error', 'No existe ninguna reparación con el código <strong class="flash">'.$code.'</strong>.');

            return $this->redirectToRoute('repair_query');
        }

        return $this->render('vista-de-reparacion.html.twig', array(
            'receipt' => $receipt
        ));
    }

    /**
     * Genera y procesa el calendario de cursos
     */
    public function calendar()
    {
//        $em = $this->getDoctrine()->getManager();
//        $receipt = $em->getRepository('App:Receipt')->findOneByCode($code);

        return $this->render('calendario.html.twig');
    }
}