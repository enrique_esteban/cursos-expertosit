<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadTags extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Etiqueta
        $tag = new Tag();

        $tag->setName('Youtube');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-youtube', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Video');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-video', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Curso');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-curso', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Clase');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-clase', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('iPhone');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-iphone', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Cámara térmica');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-camara-termica', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Seek Thermal');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-seek-thermal', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Backlight');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-backlight', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Reparación');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-reparacion', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Fuente de alimentación');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-fuente-alimentacion', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Fuente de alimentación portátil');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-fuente-alimentacion-portatil', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Diagnóstico');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-diagnostico', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Botón home');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-boton-home', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Dock test');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-dock-test', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('iPhone 7');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-iphone-7', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Camara');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-camara', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Cortocircuito');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-corto', $tag); // Fin de la etiqueta

        $manager->flush();

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Micro-SD');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-micro-sd', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Pantalla');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-pantalla', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Alumnos');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-alumnos', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('iPhone 6');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-iphone-6', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Reballing');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-reballing', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('IC');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-ic', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Samsung');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-samsung', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Herramienta');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-herramienta', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Aula');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-aula', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Soldar');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-soldar', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Tienda');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-tienda', $tag); // Fin de la etiqueta

        // Etiqueta
        $tag = new Tag();

        $tag->setName('Taller');
        $tag->setDescription(null);

        $manager->persist($tag);
        $this->setReference('tag-taller', $tag); // Fin de la etiqueta

        $manager->flush();


    }
}