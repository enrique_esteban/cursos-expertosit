<?php

namespace App\Form;

use App\Entity\Receipt;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type as Type;

/**
 * Formulario para la consulta de dispositivos.
 * 
 * @author Enrique José Esteban Plaza <ense.esteban@gmail.com>
 */
class RepairQueryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', @Type\TextType::class, array(
                'label' => 'Código'
            ))
            ->add('submit', @Type\SubmitType::class, array(
                'label' => 'Enviar'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Receipt::class,
        ));
    }

    public function getBlockPrefix()
    {
        return 'app-repair-query';
    }
}