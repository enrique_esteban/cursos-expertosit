<?php

namespace App\DataFixtures;

use App\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadImages extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Vista trasera del aula de los cursos');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('aula-curso-01.jpg');
        $image->setAlt('vista trasera aula de cursos');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-aula'));
        $image->addTag($this->getReference('tag-clase'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Vista frontal del aula de los cursos');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('aula-curso-02.jpg');
        $image->setAlt('vista frontal aula de cursos');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-aula'));
        $image->addTag($this->getReference('tag-clase'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Vista trasera del aula de los cursos');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('aula-curso-03.jpg');
        $image->setAlt('vista trasera aula de cursos');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-aula'));
        $image->addTag($this->getReference('tag-clase'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Cámara térmica SeekThermal');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('camara-termica-01.jpg');
        $image->setAlt('camara termica');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-camara'));
        $image->addTag($this->getReference('tag-camara-termica'));
        $image->addTag($this->getReference('tag-taller'));
        $image->addTag($this->getReference('tag-aula'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Cubeta de ultrasonidos');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('cubeta-ultrasonidos-01.jpg');
        $image->setAlt('cubeta de ultrasonidos');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-taller'));
        $image->addTag($this->getReference('tag-aula'));

        $manager->persist($image); // fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Desoldando un IC en un iPhone 6S');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('desoldando-01.jpg');
        $image->setAlt('desoldando ic en iphone 6S');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-soldar'));
        $image->addTag($this->getReference('tag-taller'));
        $image->addTag($this->getReference('tag-soldar'));

        $manager->persist($image); // fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Estación de carga múltiple');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('estacion-de-carga-01.jpg');
        $image->setAlt('estación de carga multiple');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-aula'));
        $image->addTag($this->getReference('tag-taller'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Estación de soldadura del curso');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('estacion-soldadura-01.jpg');
        $image->setAlt('estacion de soldadura del curso');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-soldar'));
        $image->addTag($this->getReference('tag-taller'));
        $image->addTag($this->getReference('tag-aula'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Otra estación de soldadura del curso');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('estacion-soldadura-02.jpg');
        $image->setAlt('otra estacion de soldadura');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-soldar'));
        $image->addTag($this->getReference('tag-taller'));
        $image->addTag($this->getReference('tag-aula'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Fuente de alimentación del curso');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('fuente-alimentacion-01.jpg');
        $image->setAlt('fuente alimentacion del curso');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-taller'));

        $manager->persist($image); // fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Fuente de alimentación secundaria');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('fuente-alimentacion-02.jpg');
        $image->setAlt('fuente alimentacion del curso secundaria');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-taller'));

        $manager->persist($image); // fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Herramientas de soldadura');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('herramientas-soldadura-01.jpg');
        $image->setAlt('Herramientas de soldadura 1');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-taller'));
        $image->addTag($this->getReference('tag-aula'));
        $image->addTag($this->getReference('tag-soldar'));

        $manager->persist($image); // fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Herramientas de soldadura');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('herramientas-soldadura-02.jpg');
        $image->setAlt('Herramientas de soldadura 2');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-taller'));
        $image->addTag($this->getReference('tag-aula'));
        $image->addTag($this->getReference('tag-soldar'));

        $manager->persist($image); // fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Interior de un iMac');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('interior-imac-01.jpg');
        $image->setAlt('interior de un imac');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-taller'));
        $image->addTag($this->getReference('tag-aula'));

        $manager->persist($image); // fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('iPhone 6 desmontado');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('iphone-6-desmontado-01.jpg');
        $image->setAlt('iphone 6 desmontado');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-taller'));
        $image->addTag($this->getReference('tag-aula'));

        $manager->persist($image); // fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('iPhones a la venta');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('iphones-01.jpg');
        $image->setAlt('iphones para venta');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-tienda'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Manta térmica');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('manta-termica-01.jpg');
        $image->setAlt('manta termica');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-aula'));
        $image->addTag($this->getReference('tag-taller'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Microscopio para el curso');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('miscroscopio-01.jpg');
        $image->setAlt('microscopio para curso');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-taller'));
        $image->addTag($this->getReference('tag-aula'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Microscopio para el curso');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('miscroscopio-02.jpg');
        $image->setAlt('microscopio para curso');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-taller'));
        $image->addTag($this->getReference('tag-aula'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Microscopio para el curso');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('miscroscopio-03.jpg');
        $image->setAlt('microscopio para curso');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-taller'));
        $image->addTag($this->getReference('tag-aula'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Microscopio para el curso');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('miscroscopio-04.jpg');
        $image->setAlt('microscopio para curso');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-taller'));
        $image->addTag($this->getReference('tag-aula'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Microscopio para el curso');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('miscroscopio-05.jpg');
        $image->setAlt('microscopio para curso');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-taller'));
        $image->addTag($this->getReference('tag-aula'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Osciloscopio para el curso');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('osciloscopio-01.jpg');
        $image->setAlt('Osciloscopio para curso');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-aula'));
        $image->addTag($this->getReference('tag-taller'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Distintas placas de iPhone');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('placas-iphone-01.jpg');
        $image->setAlt('distintas placas de iphone');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-aula'));
        $image->addTag($this->getReference('tag-taller'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Plancha separadora para el curso');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('plancha-separadora-01.jpg');
        $image->setAlt('cubeta de ultrasonidos');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-aula'));
        $image->addTag($this->getReference('tag-taller'));

        $manager->persist($image); // Fin Imagen



        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Taller exterior');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('taller-exterior-01.jpg');
        $image->setAlt('vista taller exterior 1');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-taller'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Taller exterior');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('taller-exterior-02.jpg');
        $image->setAlt('vista taller exterior 2');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-taller'));

        $manager->persist($image); // Fin Imagen

        // Imagen Nueva
        $image = new Image();

        $image->setTitle('Taller interior para los cursos');
        $image->setUrl('/images/pages/portfolio/');
        $image->setName('taller-interior-01.jpg');
        $image->setAlt('vista taller cursos');
        $image->setCreatedAt(new \DateTime('now'));
        $image->setIsPrivate(false);
        $image->addTag($this->getReference('tag-herramienta'));
        $image->addTag($this->getReference('tag-taller'));
        $image->addTag($this->getReference('tag-aula'));

        $manager->persist($image); // Fin Imagen

        $manager->flush();
    }

    function getDependencies()
    {
        return array(
            LoadCategories::class,
            LoadTags::Class
        );
    }
}