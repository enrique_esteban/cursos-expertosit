<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategories extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $category = new Category();

        $category->setName('Youtube');
        $category->setDescription('Vídeos subidos a nuestro canal de Youtube');

        $manager->persist($category);
        $this->setReference('cat-youtube', $category);

        $manager->flush();

    }
}