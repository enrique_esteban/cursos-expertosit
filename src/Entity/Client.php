<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="Client")
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     *
     * @Assert\NotBlank(message="Debe introducir su nombre.")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=15)
     *
     * @Assert\NotBlank(message="Debe introducir un número de teléfono.")
     * @Assert\Regex(pattern="/^((\+?34([ \t|\-])?)?[9|6|7]((\d{1}([ \t|\-])?[0-9]{3})|(\d{2}([ \t|\-])?[0-9]{2}))([ \t|\-])?[0-9]{2}([ \t|\-])?[0-9]{2})$/", message="El número {{ value }} no tiene un formato valido.")
     */
    private $phone;

    /**
     * @ORM\Column(name="create_at", type="datetime")
     */
    private $createAt;

    /**
     * @ORM\Column(type="string", length=191, unique=true)
     *
     * @Assert\NotBlank(message="Debe introducir un correo electrónico válido.")
     * @Assert\Email(
     *     message = "El correo electrónico {{ value }} no tiene un formato válido (usuario@dominio).",
     *     checkMX = true
     * )
     */
    private $email;

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCreateAt(): ?\DateTime
    {
        return $this->createAt;
    }

    public function setCreateAt($createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * HasLifecycleCallbacks: Establecemos algunos valores por defecto antes de persistir la entidad
     *
     * @ORM\PrePersist()
     */
    public function setCreatedAtValue()
    {
//        $date = new \DateTime('now');

        $this->createAt = new \DateTime('now');

    }
}
