<?php

namespace App\Repository;

use App\Entity\Entry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Entry|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entry|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entry[]    findAll()
 * @method Entry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EntryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Entry::class);
    }

    public function findRecentYoutubeVideos()
    {
        return $this->createQueryBuilder('e')
            ->innerJoin('e.category', 'c')
            ->addSelect('e')
            ->andWhere('c.name = :name')
            ->andWhere('e.isPrivated = :private')
            ->setParameters(array(
                'name' => 'Youtube',
                'private' => false,
            ))
            ->orderBy('e.createAt', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findYoutubeVideos()
    {
        return $this->createQueryBuilder('e')
            ->innerJoin('e.category', 'c')
            ->innerJoin('e.tags', 't')
            ->addSelect('e')
            ->andWhere('c.name = :name')
            ->andWhere('e.isPrivated = :private')
            ->setParameters(array(
                'name' => 'Youtube',
                'private' => false,
            ))
            ->orderBy('e.createAt', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?Entry
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
